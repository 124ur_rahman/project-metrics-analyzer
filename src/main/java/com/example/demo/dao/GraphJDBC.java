package com.example.demo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Driver;

import com.example.demo.business.MetricsGraph;


public class GraphJDBC{
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/Project_Metrics_Details?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	public List<MetricsGraph> ListMetrics(String metric_name, String project_id){
		List<MetricsGraph> metric = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			String sql = "select Month, MetricValue from ProjectMetrics where projectID = ? and MetricName = ?";
			PreparedStatement st = cn.prepareStatement(sql);
			st.setString(1, project_id);
			st.setString(2, metric_name);
			rs = st.executeQuery();
			while(rs.next()) {
				metric.add(new MetricsGraph(rs.getString(1), 
						Integer.parseInt(rs.getString(2))));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return metric;

	}
}


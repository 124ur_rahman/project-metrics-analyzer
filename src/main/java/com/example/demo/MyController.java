package com.example.demo;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.business.MetricsEntry;
import com.example.demo.business.MetricsEntryRepository;
import com.example.demo.business.MetricsGraph;

import com.example.demo.business.MetricsMaintenance;
import com.example.demo.business.MetricsMaintenanceRepository;
import com.example.demo.business.Project;
import com.example.demo.business.ProjectMaintenance;
import com.example.demo.business.ProjectMaintenanceRepository;
import com.example.demo.business.ProjectRepository;
import com.example.demo.dao.GraphJDBC;
import com.example.demo.dao.MySQLJDBCManagers;

@Controller
public class MyController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MyController.class);
	@Autowired
	ProjectMaintenanceRepository pmr;
	@Autowired
	MetricsMaintenanceRepository mmr;
	@Autowired
	MetricsEntryRepository mer;
	
	@Autowired
	ProjectRepository projectrepo;
	@Autowired
	private MySQLJDBCManagers repo;
	
	@RequestMapping("/")
	String homehandler() {
		return "login";
	}
	
	@RequestMapping("/homemanagers")
	String managershandler(Model model, HttpServletRequest req) {
		String user = (String)req.getSession().getAttribute("ID");
		model.addAttribute("mid",user);
		return "homemanagers";
	}
	@RequestMapping("/home")
	String techleadhandler(Model model, HttpServletRequest req) {
		String user = (String)req.getSession().getAttribute("ID");
		model.addAttribute("mid",user);
		return "home";
	}
	@RequestMapping("/login")
	String loginhandler(HttpServletRequest req, Model model) {
		String user = req.getParameter("uname");
		String password = req.getParameter("pword");
		
		req.getSession().setAttribute("managerloggedIn", true);
		req.getSession().setAttribute("ID", user);
		model.addAttribute("mid",user);
		
		if(password.equals(repo.findByName(user)))
		{
			if(user.substring(0,1).equals("M")) {
				req.getSession().setAttribute("managerloggedIn", true);
				req.getSession().setAttribute("ID", user);
				List<Project> projects = repo.getProjects(user, password);
				model.addAttribute("projects",projects);
				
				return "homemanagers";
			}
			else {
				req.getSession().setAttribute("techloggedIn", user);
				return "home";
			}
		}
		else {
			return "login";
		}
		
	}
	

	@RequestMapping("/projects")
	String Projectshandler(Model model) {
		model.addAttribute("projects", pmr.displayProjects());
		return "projects";
	}
	@RequestMapping("/metrics_maintain1")
	String metricshomehandler(Model model) {
		model.addAttribute("projects", pmr.displayProjects());
		return "metrics_maintain1";
	}
	
	@RequestMapping("/displayprojects/{manager_id}")
	String metricsprojectshandler(Model model,@PathVariable String manager_id) {
		model.addAttribute("projects", mer.getProjectsById(manager_id));
		//model.addAttribute("pid",mer.getProjectsById(manager_id).get(1).getProject_id());
		return "metrics-projects";
	}
	@RequestMapping("/displayprojectsgraph/{manager_id}")
	String metricsprojectsgraphhandler(Model model,@PathVariable String manager_id) {
		model.addAttribute("projects", mer.getProjectsById(manager_id));
		//model.addAttribute("pid",mer.getProjectsById(manager_id).get(1).getProject_id());
		return "metrics_view";
	}
	@RequestMapping("/metricsdisplay/{project_id}")
	String metricsdisplayhandler(@PathVariable String project_id,String month,Model model) {
		model.addAttribute("metric",new MetricsEntry(project_id,month));
		return "getmonth";
	}
	@RequestMapping("/metricsdisplayall/{project_id}")
	String graphmetricshandler(@PathVariable("project_id") String project_id,HttpServletRequest request,Model model) {
		request.getSession().setAttribute("pid", project_id);
		model.addAttribute("metrics",mmr.displayMetrics(project_id));
		model.addAttribute("projectid",mmr.displayMetrics(project_id).get(0).getProject_id());
		return "allmetrics";
	}
	@RequestMapping("/displaygraph/{metric_name}")
	String graphmetricsdisplayhandler(@PathVariable("metric_name") String metric_name,HttpServletRequest request,Model model) {
		String projectid=(String)request.getSession().getAttribute("pid");
		GraphJDBC graph = new GraphJDBC();
		List<MetricsGraph> mgraph = graph.ListMetrics(metric_name, projectid);
		model.addAttribute("graphdata",mgraph);
		return "chart";
	}
	@RequestMapping("/getthemonth")
	String getmetricshandler(HttpServletRequest request, Model model) {
		String project_id = request.getParameter("project_id");
		String month = request.getParameter("month");
		request.getSession().setAttribute("project_id", project_id);
		request.getSession().setAttribute("project_month", month);
		MetricsEntry me = new MetricsEntry(project_id,month);
		List<MetricsEntry> metrics = mer.getMetrics(me.getProject_id(),me.getMonth());
		LOGGER.info("****** " + metrics.get(0).getMetric_name());
		model.addAttribute("metrics",metrics);
		
		return "metricsbyid";
	}
	@RequestMapping("/editmetricbyname/{metric_name}")
	String metricsedithandler(@PathVariable String metric_name,Model model,HttpServletRequest request) {
		String projectid = (String)request.getSession().getAttribute("project_id");
		String mon = (String)request.getSession().getAttribute("project_month");
		model.addAttribute("metric",mer.getMetricsByName(projectid, metric_name,mon));
		System.out.println("########"+projectid+mon);
		return "editmetrics";
	}
	@RequestMapping("/editthemetrics")
	String metricssavehandler(HttpServletRequest request, Model model) {
		String pid=request.getParameter("project_id");
		String m=request.getParameter("month");
		MetricsEntry me=new MetricsEntry(request.getParameter("manager_id"),pid,
				request.getParameter("metric_name"),request.getParameter("metric_value"),m);
		mer.modifyMetrics(me);
		model.addAttribute("metrics",mer.getMetrics(pid, m));
		return "metricsbyid";
	}
	@RequestMapping("/addproject")
	String addprojecthandler(Model model) {
		model.addAttribute("project", new ProjectMaintenance("","","",""));
		return "addproject";
	}
	
	@RequestMapping("/addmetric/{project_id}")
	String addmetrichandler(@PathVariable("project_id") String project_id,Model model) {
		model.addAttribute("metric", new MetricsMaintenance(project_id,""));
		return "addmetric";
	}
	@RequestMapping("/add")
	String projectsaddhandler(HttpServletRequest request, Model model) {
		ProjectMaintenance pm = new ProjectMaintenance(request.getParameter("project_id"),
				request.getParameter("project_name"),request.getParameter("project_category"),
				request.getParameter("manager_name"));
		pmr.addProject(pm);
		model.addAttribute("projects",pmr.displayProjects());
		return "projects";
	}
	@RequestMapping("/addthemetric")
	String metricssaddhandler(HttpServletRequest request, Model model) {
		String project_id = request.getParameter("project_id");
		MetricsMaintenance mm = new MetricsMaintenance(project_id,
				request.getParameter("metric_name"));
		mmr.addMetric(mm);
		model.addAttribute("metrics",mmr.displayMetrics(project_id));
		model.addAttribute("projectid",mmr.displayMetrics(project_id).get(0).getProject_id());
		return "metrics";
	}
	
	@RequestMapping("/deleteproject/{project_id}")
	String projectsdeletehandler(@PathVariable("project_id") String project_id,Model model) {
		pmr.deleteProject(project_id);
		model.addAttribute("projects", pmr.displayProjects());
		return "projects";
	}
	@RequestMapping("/deletemetric")
	String metricsdeletehandler(HttpServletRequest request, Model model) {
		String project_id = request.getParameter("id");
		MetricsMaintenance mm = new MetricsMaintenance(project_id,
				request.getParameter("mname"));
		mmr.deleteMetric(mm);
		
		model.addAttribute("metrics",mmr.displayMetrics(project_id));
		model.addAttribute("projectid",mmr.displayMetrics(project_id).get(0).getProject_id());
		return "metrics";
	}
	/*@RequestMapping("/deleteproject")
	String metricsdeletehandler(Model model) {
		mmr.deleteMetric(mm);
		
		model.addAttribute("metrics",mmr.displayMetrics(project_id));
		return "metrics";
	}*/
	@RequestMapping("/edit/{project_id}")
	String projectsmodeifyhandler(@PathVariable("project_id") String project_id,Model model) {
		model.addAttribute("project",pmr.getProjectById(project_id));
		return "editproject";
	}
	@RequestMapping("/editmetricspage/{project_id}")
	String editMetricshandler(@PathVariable("project_id") String project_id,Model model) {
		model.addAttribute("metrics",mmr.displayMetrics(project_id));
		model.addAttribute("projectid",mmr.displayMetrics(project_id).get(0).getProject_id());
		return "metrics";
	}
	@RequestMapping("/save")
	String projectsavehandler(HttpServletRequest request, Model model) {
		ProjectMaintenance pm = new ProjectMaintenance(request.getParameter("project_id"),
				request.getParameter("project_name"),request.getParameter("project_category"),
				request.getParameter("manager_name"));
		pmr.modifyProject(pm);
		model.addAttribute("projects", pmr.displayProjects());
		return "projects";
	}
	
	/*@RequestMapping("/chart")
	String doChart(String month, Model model) {
		GraphJDBC dao = new GraphJDBC();
		List<MetricsGraph> metrics = dao.ListMetrics(month);
		model.addAttribute("sales", metrics);
		return "chart";
	}*/

}

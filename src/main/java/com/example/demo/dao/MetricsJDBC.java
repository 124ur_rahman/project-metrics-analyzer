package com.example.demo.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.business.MetricsMaintenance;
import com.example.demo.business.MetricsMaintenanceMethods;
import com.example.demo.business.ProjectMaintenance;

@Repository
public class MetricsJDBC implements MetricsMaintenanceMethods {
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/Project_Metrics_Details?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	public String months[]= {"January","February","March","April","May","June","July","August","September","October","November","December"};
	@Override
	public List<MetricsMaintenance> displayMetrics(String project_id) {
		List<MetricsMaintenance> metrics = new ArrayList<>();
		
		Connection cn = null;
		ResultSet rs = null;
		
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			
			Statement st = cn.createStatement();
			rs = st.executeQuery("select ProjectID,MetricName from ProjectMetrics where projectid='"+project_id+"' and month='January'");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				metrics.add(new MetricsMaintenance(rs.getString(1),rs.getString(2)));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return metrics;
	}

	@Override
	public void addMetric(MetricsMaintenance mm) {
		int rows = 0;
		MetricsMaintenance mm1 =null;
		Connection cn = null;
		ResultSet rs = null;
		String name = null;
		String id = null;
		String mid=null;
		
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			id=mm.getProject_id();
			name=mm.getMetric_name();
			Statement st=cn.createStatement();
			rs=st.executeQuery("select ManagerID from Projects where ProjectID='"+id+"'");
			while(rs.next()) {
				mid=rs.getString(1);
			}
			for(int i=0;i<12;i++ ) {
			PreparedStatement st1 = cn.prepareStatement("INSERT INTO ProjectMetrics(ProjectID,MetricName,Month,ManagerID,TechLeadID) VALUES(?,?,?,?,?)");
			st1.setString(1, id);
			st1.setString(2, name);
			st1.setString(3, months[i]);
			st1.setString(4, mid);
			st1.setString(5, "T001");
			rows = st1.executeUpdate();
			}
			System.out.println(rows+" row(s) added");
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
	}

	@Override
	public void deleteMetric(MetricsMaintenance mm) {
		int rows = 0;
		MetricsMaintenance mm1 =null;
		Connection cn = null;
		ResultSet rs = null;
		String mname = null;
		String id = null;
		

		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			id=mm.getProject_id();
			mname=mm.getMetric_name();
			for(int i=0;i<12;i++) {
				PreparedStatement st1 = cn.prepareStatement("DELETE FROM ProjectMetrics WHERE ProjectID=? and MetricName=? and Month=?");
				st1.setString(1, id);
				st1.setString(2, mname);
				st1.setString(3, months[i]);
				
				rows = st1.executeUpdate();
				}
			
            System.out.println(rows+" row(s) are deleted");
		} 
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
	}

}

package com.example.demo.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;


import com.example.demo.business.ProjectMaintenance;
import com.example.demo.business.ProjectMaintenanceMethods;




@Repository
public class ProjectsJDBC implements ProjectMaintenanceMethods{
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/Project_Metrics_Details?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	public String months[]= {"January","February","March","April","May","June","July","August","September","October","November","December"};
	@Override
	public List<ProjectMaintenance> displayProjects() {
		List<ProjectMaintenance> projects = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			Statement st = cn.createStatement();
			rs = st.executeQuery("select p.ProjectID, p.ProjectName, p.ProjectCategory,m.ManagerName from Projects p JOIN \r\n" + 
					"Managers m ON p.ManagerID = m.ManagerID JOIN TechnicalLead tl ON m.ManagerID = tl.ManagerID");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				projects.add(new ProjectMaintenance(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return projects;
	}

	@Override
	public void addProject(ProjectMaintenance pm) {
		int rows = 0;
		int rows1 = 0;
		ProjectMaintenance pm1 =null;
		Connection cn = null;
		ResultSet rs = null;
		String name = null;
		String id = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			name=pm.getManager_name();
			Statement st=cn.createStatement();
			rs=st.executeQuery("select ManagerID from Managers where ManagerName='"+name+"'");
			while(rs.next()) {
				id=rs.getString(1);
			}
			PreparedStatement st1 = cn.prepareStatement("INSERT INTO Projects VALUES(?,?,?,?)");
			st1.setString(1, pm.getProject_id());
			st1.setString(2, pm.getProject_name());
			st1.setString(3, pm.getProject_category());
			st1.setString(4, id);
			rows = st1.executeUpdate();
			for(int i=0;i<12;i++) {
				PreparedStatement st2 = cn.prepareStatement("INSERT INTO ProjectMetrics(ProjectID,Month,ManagerID) VALUES(?,?,?)");
				st2.setString(1, pm.getProject_id());
				st2.setString(2,months[i]);
				st2.setString(3, id);
				rows1 = st2.executeUpdate();
			}
			System.out.println(rows+" row(s) added");
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
	}

	@Override
	public void deleteProject(String project_id) {
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("DELETE FROM Projects WHERE ProjectID = ?");
			PreparedStatement st1 = cn.prepareStatement("DELETE FROM ProjectMetrics WHERE ProjectID = ?");
			st.setString(1, project_id);
			st1.setString(1, project_id);
			int rows = st.executeUpdate(); 
			int rows1 = st1.executeUpdate();
            System.out.println(rows+" row(s) are deleted");
            System.out.println(rows1+" row(s) are deleted");
		} 
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
		
	}

	@Override
	public ProjectMaintenance modifyProjects(ProjectMaintenance pm) {
		int rows = 0;
		ProjectMaintenance pm1 =null;
		Connection cn = null;
		ResultSet rs = null;
		String id=null;
		String name=null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			name=pm.getManager_name();
			Statement st=cn.createStatement();
			rs=st.executeQuery("select ManagerID from Managers where ManagerName='"+name+"'");
			while(rs.next()) {
				id=rs.getString(1);
			}
			PreparedStatement st1 = cn.prepareStatement("UPDATE Projects SET ProjectID = ?, ProjectName = ?,ProjectCategory = ?,ManagerID = '"+id+"' WHERE " + 
			"ProjectID = ?");
			st1.setString(1, pm.getProject_id());
			st1.setString(2, pm.getProject_name());
			st1.setString(3, pm.getProject_category());
			st1.setString(4, pm.getProject_id());
			rows = st1.executeUpdate();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return null;
	}
	@Override
	public ProjectMaintenance getProjectById(String project_id) {
		ProjectMaintenance pm1 =null;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("select p.ProjectID,p.ProjectName, p.ProjectCategory,m.ManagerName from Projects p JOIN \r\n" + 
					"Managers m ON p.ManagerID = m.ManagerID JOIN TechnicalLead tl ON m.ManagerID = tl.ManagerID where p.ProjectID=?");
			st.setString(1, project_id);
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				pm1 = new ProjectMaintenance(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return pm1;
		
	}

	
}

package com.example.demo.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;


import com.example.demo.business.Project;
import com.example.demo.business.ProjectData;


@Repository
public class MySQLJDBCManagers implements ProjectData {
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/Project_Metrics_Details?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	
	@Override
	public List<Project> getProjects(String managerID, String Password) {
		// TODO Auto-generated method stub
		List<Project> projects = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			String sql = 
				"SELECT p.ProjectID, p.ProjectName,p.Projectcategory,p.ManagerID from Projects p \r\n" + 
				" JOIN Managers m on m.ManagerID=p.managerID WHERE p.ManagerID=? and m.Password=?;";
			
			PreparedStatement st = cn.prepareStatement(sql);
			st.setString(1, managerID);
			st.setString(2, Password);
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			
			while (rs.next()) { 
				projects.add(new Project(rs.getInt("ProjectID"),rs.getString("ProjectName"),
						rs.getString("ProjectCategory"),rs.getString("ManagerID")));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return projects;
	}
	
	public String findByName(String Employee) {
		String result = "Not Found";
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("SELECT Password FROM vValidation "
					+ "WHERE User = ?");
			st.setString(1, Employee);
			rs = st.executeQuery();
			if(rs.next()) {
				return rs.getString(1);
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
	}
	
}


	
	
	
	
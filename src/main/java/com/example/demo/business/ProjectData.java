package com.example.demo.business;

import java.util.List;

public interface ProjectData {
	List<Project> getProjects(String managerID, String Password);
}

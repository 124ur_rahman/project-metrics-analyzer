package com.example.demo.business;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ProjectMaintenanceRepository {
	private ProjectMaintenanceMethods pmm;
	@Autowired
	public ProjectMaintenanceRepository(ProjectMaintenanceMethods project_methods) {
		this.pmm=project_methods;
	}
	public List<ProjectMaintenance> displayProjects() {
		return pmm.displayProjects();
	}
	public void addProject(ProjectMaintenance pm) {
		pmm.addProject(pm);
	}
	public void deleteProject(String project_id) {
		pmm.deleteProject(project_id);
	}
	public ProjectMaintenance modifyProject(ProjectMaintenance pm) {
		return pmm.modifyProjects(pm);
    }
	public ProjectMaintenance getProjectById(String project_id) {
		return pmm.getProjectById(project_id);
	}
}

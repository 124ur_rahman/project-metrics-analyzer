create database Project_Metrics_Details;
use Project_Metrics_Details;
SET SQL_SAFE_UPDATES=0;
 -- Creating Projects table
create table Projects(ProjectID int not null,
ProjectName varchar(50) not null, ProjectCategory varchar(50),
ManagerID varchar(10), primary key(ProjectID)
);
insert into Projects values(1,"FX-Bench","FX","M001");
insert into Projects values(2,"e-Dealer Web","e-Trading","M001");
insert into Projects values(3,"FX-Pulse","FX","M002");
insert into Projects values(4,"Sonar Dealer","e-Trading","M002");
insert into Projects values(5,"Aggregator Risk","Risk","M003");
insert into Projects values(6,"Bond Distribution","Sales","M003");

create table Managers(
ManagerID varchar(10),ManagerName varchar(50),Password varchar(50),
primary key(ManagerID)
);
insert into Managers values("M001","Aldam","MA123");
insert into Managers values("M002","Bala","MB123");
insert into Managers values("M003","Carol","MC123");


create table TechnicalLead(
ManagerID varchar(10), TechLeadID varchar(10), TechLeadName varchar(50),Password varchar(50)
);
insert into TechnicalLead values("M001","T001","Santosh","TS123");
insert into TechnicalLead values("M002","T001","Santosh","TS123");
insert into TechnicalLead values("M003","T001","Santosh","TS123");

CREATE VIEW vValidation As
SELECT DISTINCT TechLeadID User, Password FROM TechnicalLead
UNION
SELECT ManagerID, Password FROM Managers;




create table ProjectMetrics(
ProjectID int, MetricName varchar(30), MetricValue varchar(30),Month varchar(30),
ManagerID varchar(30),TechLeadID varchar(30)
);

-- projectid=1
insert into ProjectMetrics values (1, "Complexity", "50%", "January","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "100000", "January","M001","T001");
insert into ProjectMetrics values (1, "Comments", "50%", "January","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "January","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "40%", "February","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "90000", "February","M001","T001");
insert into ProjectMetrics values (1, "Comments", "60%", "February","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "20%", "February","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "35%", "March","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "80000", "March","M001","T001");
insert into ProjectMetrics values (1, "Comments", "60%", "March","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "25%", "March","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "30%", "April","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "70000", "April","M001","T001");
insert into ProjectMetrics values (1, "Comments", "60%", "April","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "20%", "April","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "30%", "May","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "60000", "May","M001","T001");
insert into ProjectMetrics values (1, "Comments", "60%", "May","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "20%", "May","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "25%", "June","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "50000", "June","M001","T001");
insert into ProjectMetrics values (1, "Comments", "65%", "June","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "20%", "June","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "50%", "July","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "40000", "July","M001","T001");
insert into ProjectMetrics values (1, "Comments", "50%", "July","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "July","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "20%", "August","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "30000", "August","M001","T001");
insert into ProjectMetrics values (1, "Comments", "50%", "August","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "August","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "20%", "September","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "20000", "September","M001","T001");
insert into ProjectMetrics values (1, "Comments", "60%", "September","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "September","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "30%", "October","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "20000", "October","M001","T001");
insert into ProjectMetrics values (1, "Comments", "50%", "October","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "October","M001","T001");

insert into ProjectMetrics values (1, "Complexity", "20%", "November","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "10000", "November","M001","T001");
insert into ProjectMetrics values (1, "Comments", "65%", "November","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "30%", "November","M001","T001");


insert into ProjectMetrics values (1, "Complexity", "10%", "December","M001","T001");
insert into ProjectMetrics values (1, "LinesOfCode", "10000", "December","M001","T001");
insert into ProjectMetrics values (1, "Comments", "65%", "December","M001","T001");
insert into ProjectMetrics values (1, "DuplicatedLines", "10%", "December","M001","T001");

-- projectid=2
insert into ProjectMetrics values (2, "Issues", "70000", "January","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "100000", "January","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1000", "January","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "January","M001","T001");

insert into ProjectMetrics values (2, "Issues", "60000", "February","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "90000", "February","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1050", "February","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "40", "February","M001","T001");

insert into ProjectMetrics values (2, "Issues", "50000", "March","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "40000", "March","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "2000", "March","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "50", "March","M001","T001");

insert into ProjectMetrics values (2, "Issues", "40000", "April","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "50000", "April","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "2000", "April","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "April","M001","T001");

insert into ProjectMetrics values (2, "Issues", "30000", "May","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "10000", "May","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1500", "May","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "May","M001","T001");

insert into ProjectMetrics values (2, "Issues", "20000", "June","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "9000", "June","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1000", "June","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "20", "June","M001","T001");

insert into ProjectMetrics values (2, "Issues", "9000", "July","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "9000", "July","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1000", "July","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "July","M001","T001");

insert into ProjectMetrics values (2, "Issues", "9000", "August","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "8000", "August","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1020", "August","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "20", "August","M001","T001");

insert into ProjectMetrics values (2, "Issues", "8000", "September","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "8000", "September","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1000", "September","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "September","M001","T001");

insert into ProjectMetrics values (2, "Issues", "7000", "October","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "7000", "October","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1500", "October","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "October","M001","T001");

insert into ProjectMetrics values (2, "Issues", "6000", "November","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "7000", "November","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1460", "November","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "November","M001","T001");

insert into ProjectMetrics values (2, "Issues", "5000", "December","M001","T001");
insert into ProjectMetrics values (2, "Bugs", "5000", "December","M001","T001");
insert into ProjectMetrics values (2, "SizeOfFiles", "1000", "December","M001","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "20", "December","M001","T001");

-- Manager 2
-- Project 3

insert into ProjectMetrics values (3,"Vulnerability","32%","January","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","4","January","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","56%","January","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","65%","January","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","12%","February","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","5","February","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","76%","February","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","55%","February","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","49%","March","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","2","March","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","67%","March","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","47%","March","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","33%","April","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","6","April","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","59%","April","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","77%","April","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","19%","May","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","3","May","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","88%","May","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","73%","May","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","33%","June","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","4","June","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","57%","June","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","45%","June","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","22%","July","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","5","July","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","78%","July","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","56%","July","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","11%","August","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","8","August","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","87%","August","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","83%","August","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","65%","September","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","4","September","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","72%","September","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","61%","September","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","17%","October","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","4","October","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","77%","October","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","55%","October","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","32%","November","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","4","November","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","56%","November","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","65%","November","M002","T001");

 insert into ProjectMetrics values (3,"Vulnerability","54%","December","M002","T001");
insert into ProjectMetrics values (3,"TeamSize","3","December","M002","T001");
insert into ProjectMetrics values (3,"MaintainabilityRating","44%","December","M002","T001");
insert into ProjectMetrics values (3,"TestCaseCoverage","88%","December","M002","T001");

 -- Project 4
 
insert into ProjectMetrics values (4,"Complexity","78597","January","M002","T001");
insert into ProjectMetrics values (4,"Comments","66%","January","M002","T001");
insert into ProjectMetrics values (4,"Issues","65967","January","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","11","January","M002","T001");

insert into ProjectMetrics values (4,"Complexity","34563","February","M002","T001");
insert into ProjectMetrics values (4,"Comments","36%","February","M002","T001");
insert into ProjectMetrics values (4,"Issues","98788","February","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","31","February","M002","T001");
 
 insert into ProjectMetrics values (4,"Complexity","12324","March","M002","T001");
insert into ProjectMetrics values (4,"Comments","45%","March","M002","T001");
insert into ProjectMetrics values (4,"Issues","89787","March","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","49","March","M002","T001");

insert into ProjectMetrics values (4,"Complexity","87493","April","M002","T001");
insert into ProjectMetrics values (4,"Comments","39%","April","M002","T001");
insert into ProjectMetrics values (4,"Issues","165967","April","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","43","April","M002","T001");

insert into ProjectMetrics values (4,"Complexity","38597","May","M002","T001");
insert into ProjectMetrics values (4,"Comments","57%","May","M002","T001");
insert into ProjectMetrics values (4,"Issues","56422","May","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","15","May","M002","T001");

insert into ProjectMetrics values (4,"Complexity","45343","June","M002","T001");
insert into ProjectMetrics values (4,"Comments","55%","June","M002","T001");
insert into ProjectMetrics values (4,"Issues","121233","June","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","72","June","M002","T001");

insert into ProjectMetrics values (4,"Complexity","54543","July","M002","T001");
insert into ProjectMetrics values (4,"Comments","38%","July","M002","T001");
insert into ProjectMetrics values (4,"Issues","87464","July","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","15","July","M002","T001");

insert into ProjectMetrics values (4,"Complexity","45343","August","M002","T001");
insert into ProjectMetrics values (4,"Comments","64%","August","M002","T001");
insert into ProjectMetrics values (4,"Issues","133345","August","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","28","August","M002","T001");

insert into ProjectMetrics values (4,"Complexity","51283","September","M002","T001");
insert into ProjectMetrics values (4,"Comments","59%","September","M002","T001");
insert into ProjectMetrics values (4,"Issues","72322","September","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","51","September","M002","T001");

insert into ProjectMetrics values (4,"Complexity","92252","October","M002","T001");
insert into ProjectMetrics values (4,"Comments","34%","October","M002","T001");
insert into ProjectMetrics values (4,"Issues","32117","October","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","14","October","M002","T001");

insert into ProjectMetrics values (4,"Complexity","83622","November","M002","T001");
insert into ProjectMetrics values (4,"Comments","32%","November","M002","T001");
insert into ProjectMetrics values (4,"Issues","183823","November","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","44","November","M002","T001");

insert into ProjectMetrics values (4,"Complexity","64230","December","M002","T001");
insert into ProjectMetrics values (4,"Comments","44%","December","M002","T001");
insert into ProjectMetrics values (4,"Issues","23423","December","M002","T001");
insert into ProjectMetrics values (4,"SizeOfFiles","21","December","M002","T001");

-- projectid=5
insert into ProjectMetrics values (5, "LinesOfCode", "70000", "January","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "100000", "January","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1000", "January","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "January","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "60000", "February","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "90000", "February","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1050", "February","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "40", "February","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "50000", "March","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "40000", "March","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "2000", "March","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "50", "March","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "40000", "April","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "50000", "April","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "2000", "April","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "April","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "50000", "May","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "10000", "May","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1500", "May","M003","T001");
insert into ProjectMetrics values (2, "NumberOfClasses", "30", "May","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "40000", "June","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "9000", "June","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1000", "June","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "20", "June","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "30000", "July","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "9000", "July","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1000", "July","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "July","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "19000", "August","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "8000", "August","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1020", "August","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "20", "August","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "18000", "September","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "8000", "September","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1000", "September","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "September","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "17000", "October","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "7000", "October","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1500", "October","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "October","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "16000", "November","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "7000", "November","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1460", "November","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "30", "November","M003","T001");

insert into ProjectMetrics values (5, "LinesOfCode", "15000", "December","M003","T001");
insert into ProjectMetrics values (5, "Bugs", "5000", "December","M003","T001");
insert into ProjectMetrics values (5, "DuplicatedLines", "1000", "December","M003","T001");
insert into ProjectMetrics values (5, "NumberOfClasses", "20", "December","M003","T001");

-- projectid=6
insert into ProjectMetrics values (6, "Vulnerability", "50%", "January","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "January","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "50%", "January","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "30%", "January","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "40%", "February","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "February","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "60%", "February","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "30%", "February","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "35%", "March","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "March","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "60%", "March","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "40%", "March","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "30%", "April","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "April","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "60%", "April","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "40%", "April","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "30%", "May","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "May","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "60%", "May","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "50%", "May","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "25%", "June","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "June","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "65%", "June","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "50%", "June","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "50%", "July","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "July","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "50%", "July","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "60%", "July","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "20%", "August","M003","T001");
insert into ProjectMetrics values (6, "LinesOfCode", "5", "August","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "50%", "August","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "60%", "August","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "20%", "September","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "September","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "60%", "September","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "70%", "September","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "30%", "October","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "October","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "50%", "October","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "70%", "October","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "20%", "November","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "November","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "65%", "November","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "80%", "November","M003","T001");

insert into ProjectMetrics values (6, "Vulnerability", "10%", "December","M003","T001");
insert into ProjectMetrics values (6, "TeamSize", "5", "December","M003","T001");
insert into ProjectMetrics values (6, "MaintainabilityRating", "65%", "December","M003","T001");
insert into ProjectMetrics values (6, "TestCaseCoverage", "80%", "December","M003","T001");

UPDATE ProjectMetrics SET MetricValue = Left(MetricValue,2) WHERE Right(MetricValue,1) = '%';
select * from projectmetrics;

package com.example.demo.business;

public class MetricsMaintenance {
	String project_id;
	String metric_name;
	public MetricsMaintenance(String project_id, String metric_name) {
		super();
		this.project_id = project_id;
		this.metric_name = metric_name;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getMetric_name() {
		return metric_name;
	}
	public void setMetric_name(String metric_name) {
		this.metric_name = metric_name;
	}
	
	
	
}

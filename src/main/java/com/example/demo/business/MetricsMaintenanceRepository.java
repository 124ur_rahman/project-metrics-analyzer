package com.example.demo.business;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class MetricsMaintenanceRepository {
	private MetricsMaintenanceMethods mmm;
	@Autowired
	public MetricsMaintenanceRepository(MetricsMaintenanceMethods metrics_methods) {
		this.mmm=metrics_methods;
	}
	public List<MetricsMaintenance> displayMetrics(String project_id) {
		return mmm.displayMetrics(project_id);
	}
	public void addMetric(MetricsMaintenance mm) {
		mmm.addMetric(mm);
	}
	public void deleteMetric(MetricsMaintenance mm) {
		mmm.deleteMetric(mm);
	}
}

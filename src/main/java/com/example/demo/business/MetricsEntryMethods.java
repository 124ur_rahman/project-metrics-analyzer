package com.example.demo.business;
import java.util.*;
public interface MetricsEntryMethods {
	public List<MetricsEntry> getMetrics(String project_id,String month);
	public MetricsEntry getMetricsByName(String project_id,String metric_name,String month);
	public MetricsEntry modifyMetrics(MetricsEntry me);
	public List<MetricsEntry> getProjectsById(String manager_id);
}

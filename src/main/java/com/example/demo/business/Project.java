package com.example.demo.business;

public class Project {
	private int projectID;
	private String projectName;
	private String projectCategory;
	private String managerID;
	
	
	public Project(int projectID, String projectName, String projectCategory, String managerID) {
		super();
		this.projectID = projectID;
		this.projectName = projectName;
		this.projectCategory = projectCategory;
		this.managerID = managerID;
	}
	public int getProjectID() {
		return projectID;
	}
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectCategory() {
		return projectCategory;
	}
	public void setProjectCategory(String projectCategory) {
		this.projectCategory = projectCategory;
	}
	public String getManagerID() {
		return managerID;
	}
	public void setManagerID(String managerID) {
		this.managerID = managerID;
	}
	
}

package com.example.demo;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.business.MetricsMaintenance;
import com.example.demo.business.MetricsMaintenanceRepository;
import com.example.demo.business.ProjectMaintenance;
import com.example.demo.business.ProjectMaintenanceRepository;
@RestController
@CrossOrigin
public class ProjectsRestController {
	@Autowired
	ProjectMaintenanceRepository pmr;
	@Autowired
	MetricsMaintenanceRepository mmr;
	@RequestMapping(method=RequestMethod.GET, value="/displayProjects")
	public List<ProjectMaintenance> displayProjects() {
		return pmr.displayProjects();
	}
	@RequestMapping(method=RequestMethod.GET, value="/displayProjects/{project_id}")
	public List<MetricsMaintenance> displayProjects(String project_id) {
		return mmr.displayMetrics(project_id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/project")
	public void addProject(@RequestBody ProjectMaintenance pm) {
		pmr.addProject(pm);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/displayProjects")
	public void deleteProject(@PathVariable String project_id) {
		pmr.deleteProject(project_id);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/project")
	public ProjectMaintenance modifyProjects(ProjectMaintenance pm) {
		return pmr.modifyProject(pm);
	}
}

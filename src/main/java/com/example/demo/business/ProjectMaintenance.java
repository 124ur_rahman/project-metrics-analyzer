package com.example.demo.business;

public class ProjectMaintenance {
	String project_id;
	String project_name;
	String project_category;
	String manager_name;
	public ProjectMaintenance(String project_id, String project_name, String project_category, String manager_name) {
		super();
		this.project_id = project_id ;
		this.project_name = project_name;
		this.project_category = project_category;
		this.manager_name = manager_name;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getProject_category() {
		return project_category;
	}
	public void setProject_category(String project_category) {
		this.project_category = project_category;
	}
	public String getManager_name() {
		return manager_name;
	}
	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}
	
}

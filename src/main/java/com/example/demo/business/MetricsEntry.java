package com.example.demo.business;

public class MetricsEntry {
	String manager_id;
	String project_id;
	String metric_name;
	String metric_value;
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getProject_category() {
		return project_category;
	}
	public void setProject_category(String project_category) {
		this.project_category = project_category;
	}
	String month;
	String project_name;
	String project_category;
	
	public MetricsEntry(String manager_id, String project_id, String project_name, String project_category) {
		super();
		this.manager_id = manager_id;
		this.project_id = project_id;
		this.project_name = project_name;
		this.project_category = project_category;
	}
	public MetricsEntry(String metric_name, String metric_value, String month) {
		super();
		this.metric_name = metric_name;
		this.metric_value = metric_value;
		this.month = month;
	}
	
	public MetricsEntry(String manager_id, String project_id, String metric_name, String metric_value, String month) {
		super();
		this.manager_id = manager_id;
		this.project_id = project_id;
		this.metric_name = metric_name;
		this.metric_value = metric_value;
		this.month = month;
	}
	public MetricsEntry(String project_id, String month) {
		super();
		this.project_id = project_id;
		this.month = month;
	}
	
	public String getManager_id() {
		return manager_id;
	}
	public void setManager_id(String manager_id) {
		this.manager_id = manager_id;
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getMetric_name() {
		return metric_name;
	}
	public void setMetric_name(String metric_name) {
		this.metric_name = metric_name;
	}
	public String getMetric_value() {
		return metric_value;
	}
	public void setMetric_value(String metric_value) {
		this.metric_value = metric_value;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	
}

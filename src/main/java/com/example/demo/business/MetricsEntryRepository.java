package com.example.demo.business;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class MetricsEntryRepository {
	private MetricsEntryMethods mem;
	@Autowired
	public MetricsEntryRepository(MetricsEntryMethods metrics_m) {
		this.mem=metrics_m;
	}
	public List<MetricsEntry> getMetrics(String project_id,String month) {
		return mem.getMetrics(project_id,month);
	}
	
	public MetricsEntry modifyMetrics(MetricsEntry me) {
		return mem.modifyMetrics(me);
	}
	public List<MetricsEntry> getProjectsById(String manager_id) {
		return mem.getProjectsById(manager_id);
	}
	public MetricsEntry getMetricsByName(String project_id,String metric_name,String month) {
		return mem.getMetricsByName(project_id, metric_name,month);
	}
}

package com.example.demo.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.business.MetricsEntry;
import com.example.demo.business.MetricsEntryMethods;
import com.example.demo.business.MetricsMaintenance;
import com.example.demo.business.Project;


@Repository
public class MetricsEntryJDBC implements MetricsEntryMethods {
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/Project_Metrics_Details?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	@Override
	public List<MetricsEntry> getMetrics(String project_id,String month) {
   List<MetricsEntry> metrics = new ArrayList<>();
		
		Connection cn = null;
		ResultSet rs = null;
		
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			
			Statement st = cn.createStatement();
			rs = st.executeQuery("select MetricName,MetricValue,Month from ProjectMetrics where projectid='"+project_id+"' and month='"+month+"'");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				metrics.add(new MetricsEntry(rs.getString(1),rs.getString(2),rs.getString(3)));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return metrics;
	}

	

	@Override
	public MetricsEntry modifyMetrics(MetricsEntry me) {
		int rows = 0;
		MetricsEntry me1 =null;
		Connection cn = null;
		ResultSet rs = null;
		String id=null;
		String name=null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st1 = cn.prepareStatement("UPDATE ProjectMetrics SET MetricValue=? WHERE ProjectID=? and MetricName=? and Month=?");

			st1.setString(1, me.getMetric_value());
			st1.setString(2, me.getProject_id());
			st1.setString(3, me.getMetric_name());
			st1.setString(4, me.getMonth());
			rows = st1.executeUpdate();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return null;
		
	}

	@Override
	public List<MetricsEntry> getProjectsById(String manager_id) {
		List<MetricsEntry> mprojects = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			Statement st = cn.createStatement();
			rs = st.executeQuery("select p.ManagerID,p.ProjectID, p.ProjectName, p.ProjectCategory from Projects p WHERE p.ManagerID='"+manager_id+"'");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				mprojects.add(new MetricsEntry(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return mprojects;
	}



	@Override
	public MetricsEntry getMetricsByName(String project_id, String metric_name,String month) {
		MetricsEntry me1 =null;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("select ManagerID,ProjectID,MetricName,MetricValue,Month from ProjectMetrics "
					+ "where ProjectID='"+project_id+"' and MetricName='"+metric_name+"' and Month='"+month+"'");
			
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				me1 = new MetricsEntry(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return me1;
		
		
	}
	


}

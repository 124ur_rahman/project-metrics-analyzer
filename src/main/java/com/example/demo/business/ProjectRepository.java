package com.example.demo.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectRepository {
	@Autowired
	ProjectData projectdata;
	
	public List<Project> getProjects(String managerID, String Password){
		return projectdata.getProjects(managerID, Password);
	}
	
}

package com.example.demo.business;
import java.util.*;
public interface MetricsMaintenanceMethods {
	public List<MetricsMaintenance> displayMetrics(String project_id);
	public void addMetric(MetricsMaintenance mm);
	public void deleteMetric(MetricsMaintenance mm);
	
}
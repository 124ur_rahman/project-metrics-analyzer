package com.example.demo.business;
import java.util.*;
public interface ProjectMaintenanceMethods {
	public List<ProjectMaintenance> displayProjects();
	public void addProject(ProjectMaintenance pm);
	public void deleteProject(String project_id);
	public ProjectMaintenance modifyProjects(ProjectMaintenance pm);
	public ProjectMaintenance getProjectById(String project_id);
}

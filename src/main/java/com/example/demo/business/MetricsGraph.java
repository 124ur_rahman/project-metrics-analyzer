package com.example.demo.business;
	
public class MetricsGraph {
	private String month;
	private int metricvalue;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getMetricvalue() {
		return metricvalue;
	}
	public void setMetricvalue(int metricvalue) {
		this.metricvalue = metricvalue;
	}
	public MetricsGraph(String month, int metricvalue) {
		super();
		this.month = month;
		this.metricvalue = metricvalue;
	}

}
